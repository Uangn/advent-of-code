import Data.Char (isNumber, ord)
import Data.List (isPrefixOf, find)

test = "1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet"
test2 = "two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen"

main :: IO ()
main = do
    fileContents <- readFile "../inputs/p1_input"

    print $ sum . map calValue . lines $ test
    print $ sum . map calValue . lines $ fileContents

    print $ sum . map realCalValue . lines $ test2
    print $ sum . map realCalValue . lines $ fileContents

digitWords :: [(String, Char)]
digitWords = zip ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"] ['1'..'9']

mergeFirstAndLastCharToNum :: [Char] -> Int
mergeFirstAndLastCharToNum nums = read [head nums, last nums]

calValue :: [Char] -> Int
calValue xs = mergeFirstAndLastCharToNum nums
    where
    nums = filter isNumber xs

realCalValue :: [Char] -> Int
realCalValue = mergeFirstAndLastCharToNum . go []
    where
    go :: [Char] -> [Char] -> [Char]
    go a [] = reverse a
    go a (x:xs) = flip go xs $
        if isNumber x
            then x:a
            else maybe a ((:a) . snd)
                $ find (flip isPrefixOf (x:xs) . fst) digitWords
