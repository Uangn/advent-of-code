import Debug.Trace (traceShowId, traceShow)
import Data.Bifunctor (first)

test =
    "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\nGame 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\nGame 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\nGame 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\nGame 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"

data Round = Round
    { red :: Int
    , green :: Int
    , blue :: Int
    } deriving (Show)

emptyRound = Round {red=0,green=0,blue=0}
redRound n = emptyRound {red=n}
greenRound n = emptyRound {green=n}
blueRound n = emptyRound {blue=n}

roundMaxOf :: Round -> Round -> Bool
roundMaxOf (Round r1 g1 b1) (Round r2 g2 b2)
    = r2 <= r1 && g2 <= g1 && b2 <= b1


data Game = Game
    { gameId :: Int
    , rounds :: [Round]
    } deriving (Show)




main :: IO ()
main = do
    fileContents <- readFile "../inputs/p2_input"
    putStrLn test

    print $ solve1 . lines $ test
    print $ solve1 . lines $ fileContents

    print $ solve2 . lines $ test
    print $ solve2 . lines $ fileContents

parseGame :: String -> Game
parseGame xs = Game (read _gameId) $ map parseColor _colors
    where
    (_gameId, postGameId) = span (/=':')
                          . tail
                          . dropWhile (/=' ')
                          $ xs
    _colors = map (takeWhile (/=';'))
            . split ((||) . (==';') <*> (==','))
            $ tail postGameId
    parseColor = getColor
               . first read
               . splitOn (==' ')
               . tail
    getColor = go
        where
        go (num,colorName) =
            case colorName of
                "red" -> redRound num
                "green" -> greenRound num
                "blue" -> blueRound num
                _ -> emptyRound


maxOfColors (Round r1 g1 b1) (Round r2 g2 b2)
    = Round (max r1 r2) (max g1 g2) (max b1 b2)

solve1 :: [String] -> Int
solve1 = sum . map gameId . filter allRoundsValid . map parseGame
    where
    maxRound = Round {red=12, green=13, blue=14}
    allRoundsValid (Game i rs) = roundMaxOf maxRound $ foldl1 maxOfColors rs

solve2 :: [String] -> Int
solve2 = sum . map (multRound . foldl1 maxOfColors . rounds . parseGame)
    where
    multRound (Round r g b) = r*g*b



safeTail :: [a] -> [a]
safeTail xs = if null xs then [] else tail xs

split :: (a -> Bool) -> [a] -> [[a]]
split p = foldl go [[]] . reverse
    where
    go (a:as) x
        | p x = []:(a:as)
        | otherwise = (x:a):as

splitOn :: (a -> Bool) -> [a] -> ([a], [a])
splitOn p = first reverse . go []
    where
    go a [] = (a,[])
    go a (x:xs)
        | p x = (a,xs)
        | otherwise = go (x:a) xs
