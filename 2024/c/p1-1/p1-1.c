#include "random.c"
#include <stdio.h>
#include <stdlib.h>

void quicksort(int *arr, size_t len);
void _quicksort(int *arr, size_t lo, size_t hi);
size_t partition(int *arr, size_t lo, size_t hi);
size_t randPivot(size_t lo, size_t hi);

__attribute__((const)) long long diff(int *xs, int *ys, size_t len);
__attribute__((const)) char *skipFirstNumber(char *ptr);

char resize_buf(int **xs, size_t arrCap);
void printArr(int *arr, size_t len);
void doit(FILE *fptr);

int main() {
  FILE *fptr;

  fptr = fopen("../../inputs/p1-input", "r");
  doit(fptr);
  fclose(fptr);

  fptr = fopen("test-input", "r");
  doit(fptr);
  fclose(fptr);

  return 0;
}

void doit(FILE *fptr) {
  size_t bufSize = 32;
  char *buf = calloc(bufSize, sizeof(*buf));
  char *temp;
  ssize_t readSize;

  size_t arrCap = 1024;
  int *xs = calloc(arrCap, sizeof(*xs));
  int *ys = calloc(arrCap, sizeof(*ys));
  size_t size = 0;

  while ((readSize = getline(&buf, &bufSize, fptr)) != -1) {
    if (++size >= arrCap) {
      arrCap *= 2;
      resize_buf(&xs, arrCap);
      resize_buf(&ys, arrCap);
    }

    xs[size - 1] = atoi(buf);
    temp = skipFirstNumber(buf);
    ys[size - 1] = atoi(temp);
  }

  quicksort(xs, size);
  quicksort(ys, size);

  printf("%lld\n", diff(xs, ys, size));

  free(ys);
  free(xs);
  free(buf);
}

__attribute__((const)) char *skipFirstNumber(char *ptr) {
  while ('0' <= *ptr && *ptr <= '9') {
    ptr++;
  }
  while (*ptr == ' ') {
    ptr++;
  }
  return ptr;
}

__attribute__((const)) long long diff(int *xs, int *ys, size_t len) {
  long long acc = 0;
  for (size_t i = 0; i < len; i++) {
    acc += abs(xs[i] - ys[i]);
  }
  return acc;
}

char resize_buf(int **xs, size_t arrCap) {
  int *temp = reallocarray(xs, arrCap, sizeof(**xs));
  if (temp == NULL) {
    perror("failed to realloc array");
    abort();
  }
  *xs = temp;
  return 1;
}

void printArr(int *arr, size_t len) {
  putc('[', stdout);
  for (size_t i = 0; i < len; i++) {
    printf("%d, ", arr[i]);
  }
  puts("]");
}

void quicksort(int *arr, size_t len) {
  if (len > 0) {
    _quicksort(arr, 0, len - 1);
  }
}

void _quicksort(int *arr, size_t lo, size_t hi) {
  if (lo < hi) {
    size_t p = partition(arr, lo, hi);
    _quicksort(arr, lo, p);
    _quicksort(arr, p + 1, hi);
  }
}

size_t partition(int *arr, size_t lo, size_t hi) {
  int temp;
  int pivot = arr[randPivot(lo, hi)];
  size_t l = lo - 1;
  size_t r = hi + 1;

  while (1) {
    do {
      l++;
    } while (arr[l] < pivot);
    do {
      r--;
    } while (arr[r] > pivot);

    if (l >= r) {
      return r;
    }

    temp = arr[l];
    arr[l] = arr[r];
    arr[r] = temp;
  }
}

size_t randPivot(size_t lo, size_t hi) {
  return (random_size_t() % (hi - lo + 1)) + lo;
}
