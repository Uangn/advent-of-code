#ifndef RANDOM_H
#define RANDOM_H

#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32) || defined(_WIN64) // Windows
#include <bcrypt.h>
#include <windows.h>
#pragma comment(lib, "bcrypt.lib") // link it here, so no compiler flag

size_t random_size_t() {
  size_t value;
  if (BCryptGenRandom(NULL, (PUCHAR)&value, sizeof(value),
                      BCRYPT_USE_SYSTEM_PREFERRED_RNG) != 0) {
    perror("BCryptGenRandom failed");
    abort();
  }
  return value;
}

#elif defined(__linux__) // Linux
#include <sys/random.h>

size_t random_size_t() {
  size_t value;
  if (getrandom(&value, sizeof(value), 0) != sizeof(value)) {
    perror("getrandom failed");
    abort();
  }
  return value;
}

#else // Unix
#include <fcntl.h>
#include <unistd.h>

size_t random_size_t() {
  size_t value;
  int fd = open("/dev/urandom", O_RDONLY);
  if (fd < 0) {
    perror("failed to open /dev/urandom");
    abort();
  }

  if (read(fd, &value, sizeof(value)) != sizeof(value)) {
    perror("failed to read /dev/urandom");
    close(fd);
    abort();
  }

  close(fd);
  return value;
}
#endif

#endif // !RANDOM_H
