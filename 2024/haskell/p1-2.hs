{-# LANGUAGE LambdaCase #-}

import Control.Monad
import Data.Bifunctor
import Data.Map as M
import Data.Maybe (fromMaybe)

test :: String
test = "3   4\n4   3\n2   5\n1   3\n3   9\n3   3"

main :: IO ()
main = do
    print . doit =<< readFile "../inputs/p1-input"

doit :: String -> Int
doit =
    uncurry checkSims
        . fmap asMap
        . unzip
        . fmap (both (read @Int) . splitOn ' ')
        . lines
  where
    checkSims samples simMap =
        sum $
            fromMaybe 0
                . (\x -> (x *) <$> M.lookup x simMap)
                <$> samples

both :: (Bifunctor p) => (a -> b) -> p a a -> p b b
both = join bimap

asMap :: [Int] -> M.Map Int Int
asMap =
    go M.empty
  where
    go acc = \case
        [] -> acc
        x : xs -> do
            let acc' = case M.lookup x acc of
                    Nothing -> M.insert x 1 acc
                    Just e -> M.insert x (e + 1) acc
            go acc' xs

splitOn :: (Eq a) => a -> [a] -> ([a], [a])
splitOn c =
    go
  where
    go = \case
        [] -> ([], [])
        x : xs
            | x == c -> ([], xs)
            | otherwise -> first (x :) (go xs)
