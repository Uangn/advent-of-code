{-# LANGUAGE LambdaCase #-}

import Control.Monad
import Data.Bifunctor
import Data.List (sort)

test = "3   4\n4   3\n2   5\n1   3\n3   9\n3   3"

main :: IO ()
main = do
    doit =<< readFile "../inputs/p1-input"
    doit test
    pure ()

doit :: String -> IO ()
doit =
    print
        . uncurry diff
        . both sort
        . unzip
        . fmap (both (read @Int) . splitOn ' ')
        . lines

both :: (Bifunctor p) => (a -> b) -> p a a -> p b b
both = join bimap

splitOn :: (Eq a) => a -> [a] -> ([a], [a])
splitOn c =
    go
  where
    go = \case
        [] -> ([], [])
        x : xs
            | x == c -> ([], xs)
            | otherwise -> first (x :) (go xs)

diff :: (Num c) => [c] -> [c] -> c
diff xs = sum . fmap abs . zipWith (-) xs
