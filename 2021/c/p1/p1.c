#include <stdio.h>
#include <stdlib.h>

int str2int(char *str, size_t size) {
  int acc = 0;
  char *end = str + size;
  while (str != end) {
    acc *= 10;
    acc += *(str++) - '0';
  }
  return acc;
}

int doit(FILE *fptr) {
  char *buf = malloc(5 * sizeof(char));
  size_t bufSize = 4;
  ssize_t readSize;

  int curr;
  int prev;
  char prevInited = 0;
  int counter = 0;

  while ((readSize = getline(&buf, &bufSize, fptr)) != -1) {
    buf[--readSize] = 0;
    curr = str2int(buf, readSize);

    if (!prevInited) {
      prevInited = 1;
    } else if (prev < curr) {
      counter++;
    }

    prev = curr;
  }

  free(buf);
  return counter;
}

int main() {
  const char *filename = "../../inputs/p1-input";
  FILE *fptr = fopen(filename, "r");
  if (fptr == NULL) {
    fprintf(stderr, "Unable to open file '%s'\n", filename);
    exit(-1);
  }

  printf("%d\n", doit(fptr));

  fclose(fptr);
  return 0;
}
