{-# LANGUAGE LambdaCase #-}

main :: IO ()
main = do
    print
        . doit
        . fmap read
        . lines
        =<< readFile "../inputs/p1-input"
    pure ()

doit :: (Num a, Ord a) => [a] -> Int
doit = length . filter (> 0) . diff

diff :: (Num a) => [a] -> [a]
diff = \case
    [] -> []
    x : xs -> zipWith (-) xs (x : xs)
