{-# LANGUAGE LambdaCase #-}

import Control.Applicative

main = do
    print $ solve sampleTest
    print . solve . fmap read . words =<< readFile "../inputs/p1-input"

sampleTest :: (Num a) => [a]
sampleTest =
    [ 1721
    , 979
    , 366
    , 299
    , 675
    , 1456
    ]

find :: (a -> Bool) -> [a] -> Maybe a
find p = \case
    [] -> Nothing
    x : xs
        | p x -> Just x
        | otherwise -> find p xs

fmapToFst :: (b -> a) -> b -> (a, b)
fmapToFst f = (\y -> (f y, y))

solve :: (Eq a, Num a) => [a] -> Maybe a
solve = \case
    [] -> Nothing
    x : xs ->
        (x *) <$> 2020 `lookup` (fmapToFst (x +) <$> xs)
            <|> solve xs
