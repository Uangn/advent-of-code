import Data.Bifunctor (second)

test = "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8"

type Range = (Int, Int)

main :: IO ()
main = do
    fileContents <- readFile "../inputs/p4_input"

    print
        $ length
        . filter (uncurry oneContains)
        . map
            ( both parseRange
            . second tail
            . span (/=','))
        . lines
        $ fileContents

    print
        $ length
        . filter (uncurry overlaps)
        . map
            ( both parseRange
            . second tail
            . span (/=','))
        . lines
        $ fileContents

overlaps :: Range -> Range -> Bool
overlaps t1@(a1,b1) t2@(a2,b2) =
    a1 `inRange` t2
    || b1 `inRange` t2
    || a2 `inRange` t1
    || b2 `inRange` t1
    where
    inRange x (a,b) = x >= a && x <= b

both :: (a -> b) -> (a, a) -> (b, b)
both f (x,y) = (f x, f y)

oneContains :: Range -> Range -> Bool
oneContains (a1,b1) (a2,b2) =
    (a1 <= a2 && b1 >= b2)
    || (a1 >= a2 && b1 <= b2)

parseRange :: String -> Range
parseRange = (\[x,y] -> (read x, read y)) . splitOn (=='-')

splitOn :: (a -> Bool) -> [a] -> [[a]]
splitOn p = foldr f []
    where
    f x [] = [[x]]
    f x (y:ys) = 
        if p x
            then []:y:ys
            else (x:y):ys
