main :: IO ()
main = do
    fileContents <- readFile "../inputs/p1_input"
    print $ maximum . map (sum . map read) . delimitBy "" . lines $ fileContents
    print $ sum . topN 3 . map (sum . map read) . delimitBy "" . lines $ fileContents
    where
    topN :: Int -> [Int] -> [Int]
    topN = (uncurry (foldl ((tail .) . flip (insert compare))) .) . splitAt
    -- topN' n = uncurry (foldl ((tail .) . flip (insert compare))) . splitAt n
    -- topN'' n = uncurry (foldl (\a x -> tail $ insert compare x a)) . splitAt n

insert :: (a -> a -> Ordering) -> a -> [a] -> [a]
insert f x = insert'
    where
    insert' [] = [x]
    insert' (y:ys) = 
        case f x y of
            LT -> x:y:ys
            EQ -> x:y:ys
            GT -> y : insert' ys

delimitBy :: Eq a => a -> [a] -> [[a]]
delimitBy d = go [[]]
    where
        go acc [] = acc
        go acc@(a:as) (x:xs)
          | x == d = go ([]:acc) xs
          | otherwise = go ((x:a):as) xs
