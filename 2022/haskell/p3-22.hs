import qualified Data.Set as S
import Data.Char (isAsciiUpper, isAsciiLower)

main :: IO ()
main = do
    fileContents <- readFile "../inputs/p3_input"
    print
        $ fmap (sum . map lettersToNumbers)
        . mapM (uncurry (find . flip S.member) . halfSetHalfList)
        . lines
        $ fileContents

    print
        $ sum
        . map (lettersToNumbers . fromJust . commonInLists)
        . chunksOf 3
        . lines
        $ fileContents

fromJust :: Maybe a -> a
fromJust (Just x) = x
fromJust Nothing = error "fromJust: Got Nothing"

findFirst :: [a] -> Maybe a
findFirst [] = Nothing
findFirst (x:_) = Just x

find :: Foldable t => (a -> Bool) -> t a -> Maybe a
find p = foldl go Nothing
    where
    go ma@(Just _) _ = ma
    go _ x = if p x then Just x else Nothing

commonInLists :: (Ord a, Eq a) => [[a]] -> Maybe a
commonInLists = findFirst . foldl1 (\a -> filter (`S.member` S.fromList a))

chunksOf :: Int -> [a] -> [[a]]
chunksOf _ [] = []
chunksOf 0 xs = [xs]
chunksOf n xs = take n xs : chunksOf n (drop n xs)

lettersToNumbers :: Char -> Int
lettersToNumbers x
    | isAsciiUpper x = fromEnum x - 65 + 27
    | isAsciiLower x = fromEnum x - 97 + 1

halfSetHalfList :: Ord a => [a] -> (S.Set a, [a])
halfSetHalfList xs = go S.empty xs xs
    where
    go set ys [] = (set, ys)
    go set ys [x] = (set, ys)
    go set (y:ys) (_:_:fs) = go (S.insert y set) ys fs
