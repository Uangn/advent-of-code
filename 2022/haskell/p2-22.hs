import Data.List (elemIndex)

main :: IO ()
main = do
    fileContents <- readFile "../inputs/p2_input"
    print
        $ sum
        . partitionFileContentsAnd
            (\[x,y] -> (+y) . (*3) $ getGameState x y)
        $ fileContents
    print
        $ sum
        . partitionFileContentsAnd
            (\[x,y] -> 3*(y-1) + drop ([2,0,1] !! (y-1)) (cycle [1..3]) !! (x-1))
        $ fileContents
    where
    partitionFileContentsAnd f = 
        map ( f
            . map (rpsLetterToNum . head)
            . words)
            . lines

rpsLetterToNum :: Char -> Int
rpsLetterToNum x = case x of
    'A' -> 1
    'B' -> 2
    'C' -> 3
    'X' -> 1
    'Y' -> 2
    'Z' -> 3

gameStateList :: [Int]
gameStateList = cycle [1,0,2]

getGameStateListAfterFirstChoice :: Int -> [Int]
getGameStateListAfterFirstChoice y = drop (negate y `mod` 3) gameStateList

getGameState :: Int -> Int -> Int
getGameState x y = getGameStateListAfterFirstChoice y !! x

{-
 - Rock    =A=X=1 -> 1
 - Paper   =B=Y=2 -> 2
 - Scissors=C=Z=3 -> 3
 - L = 0
 - D = 3
 - W = 6
 -  abc
 - 1417
 - 2852
 - 3396
 -  abc
 - 1111
 - 2222
 - 3333
 -  abc
 - 1306
 - 2630
 - 3063
 -}
